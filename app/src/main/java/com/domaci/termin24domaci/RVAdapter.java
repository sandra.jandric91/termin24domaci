package com.domaci.termin24domaci;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.domaci.termin24domaci.fragmenti.ListFragmentRV;

import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.MyViewHolder> {

    private List<String> listaJela;
    private ListFragmentRV.OnJeloClickedListener listener;


    public RVAdapter(ListFragmentRV.OnJeloClickedListener listener, List<String> listaJela) {
        this.listener = listener;
        this.listaJela = listaJela;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view, viewGroup, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        final int pos = position;
        myViewHolder.tvTextCard.setText(listaJela.get(position));
        myViewHolder.tvTextCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onJeloClicked(pos);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaJela.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvTextCard;

        public MyViewHolder(View view) {
            super(view);
            tvTextCard = (TextView) view.findViewById(R.id.tvTextCard);
        }
    }
}
