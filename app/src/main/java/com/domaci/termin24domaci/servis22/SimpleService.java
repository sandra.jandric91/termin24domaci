package com.domaci.termin24domaci.servis22;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class SimpleService extends Service {

    public static final String CONNECTION_TYPE = "connection type";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        int status = intent.getIntExtra(CONNECTION_TYPE, 0);
        new SimpleSyncTask(getApplicationContext()).execute();

        stopSelf();
        return START_NOT_STICKY;
    }
}
