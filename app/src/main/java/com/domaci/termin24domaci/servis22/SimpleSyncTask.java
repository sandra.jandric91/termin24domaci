package com.domaci.termin24domaci.servis22;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.domaci.termin24domaci.ReviewerTools;

public class SimpleSyncTask extends AsyncTask<Void, Void, Void> {

    private Context context;

    public SimpleSyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected Void doInBackground(Void... params) {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        Toast.makeText(context, "Uredjan povezan na " + ReviewerTools.getConnectionType(ReviewerTools.getConnectivityStatus(context)), Toast.LENGTH_LONG).show();
    }
}
