package com.domaci.termin24domaci.servis23;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class KomentarService extends Service {

    public static final String KOMENTAR = "komentar";


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String komentarZaNotif = intent.getStringExtra(KOMENTAR);
        new KomentarSyncTask(komentarZaNotif, getApplicationContext()).execute();

        stopSelf();
        return START_NOT_STICKY;
    }
}
