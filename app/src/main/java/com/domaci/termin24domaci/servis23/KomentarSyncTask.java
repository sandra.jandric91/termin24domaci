package com.domaci.termin24domaci.servis23;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

public class KomentarSyncTask extends AsyncTask<Integer, Void, Integer> {

    private String komentarZaNotif;
    private Context context;

    public KomentarSyncTask(String komentarZaNotif, Context context) {
        this.komentarZaNotif = komentarZaNotif;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        komentarZaNotif = KomentarService.KOMENTAR;
    }

    @Override
    protected Integer doInBackground(Integer... params) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        Intent intent = new Intent("KOMENTAR");
        intent.putExtra("KOMENTAR_ZA_NOTIF", komentarZaNotif);
        context.sendBroadcast(intent);
    }
}
