package com.domaci.termin24domaci.model;

public class Jelo {

    private int id;
    private String imeslike;
    private String naziv;
    private String opis;
    private String kategorija;
    private String sastojci;
    private int kalorijskaVrednost;
    private double cena;
    private float rating;

    public Jelo() {

    }

    public Jelo(int id, String imeslike, String naziv, String opis, String kategorija, String sastojci, int kalorijskaVrednost, double cena, float rating) {
        this.id = id;
        this.imeslike = imeslike;
        this.naziv = naziv;
        this.opis = opis;
        this.kategorija = kategorija;
        this.sastojci = sastojci;
        this.kalorijskaVrednost = kalorijskaVrednost;
        this.cena = cena;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImeslike() {
        return imeslike;
    }

    public void setImeslike(String imeslike) {
        this.imeslike = imeslike;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public String getSastojci() {
        return sastojci;
    }

    public void setSastojci(String sastojci) {
        this.sastojci = sastojci;
    }

    public int getKalorijskaVrednost() {
        return kalorijskaVrednost;
    }

    public void setKalorijskaVrednost(int kalorijskaVrednost) {
        this.kalorijskaVrednost = kalorijskaVrednost;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return this.naziv;
    }
}
