package com.domaci.termin24domaci.model;

import com.domaci.termin24domaci.model.Jelo;

import java.util.ArrayList;
import java.util.List;

public class JeloProvider {

    public static List<Jelo> getJela() {

        List<Jelo> jela = new ArrayList<>();
        jela.add(new Jelo(0, "ovsenakasa.jpg","Ovsena kaša", "Ovsena kaša sa bananama", "Doručak", "Ovas, banane", 120, 70.00, 3));
        jela.add(new Jelo(1, "pohovanapiletina.jpg","Pohovana piletina", "Piletina sa pomfritom", "Ručak", "Piletina, krompir", 470, 350.99, 4));
        jela.add(new Jelo(2, "ribljacorba.jpg","Riblja čorba", "Čorba sa ribom", "Večera", "Voda, riba, začini", 280, 180.90, 4));
        jela.add(new Jelo(3, "cokoladnikolac.jpg","Čokoladni kolač", "Kolač sa čokoladom", "Dezert", "Brašno, čokolada, šećer", 250, 220.00, 5));
        return jela;
    }

    public static List<String> getImeJela() {

        List<String> imeJela = new ArrayList<>();
        imeJela.add("Ovsena kaša");
        imeJela.add("Pohovana piletina");
        imeJela.add("Riblja čorba");
        imeJela.add("Čokoladni kolač");
        return imeJela;
    }

    public static List<String> getKategorija() {

        List<String> kategorija = new ArrayList<>();
        kategorija.add("Doručak");
        kategorija.add("Ručak");
        kategorija.add("Večera");
        kategorija.add("Dezert");
        return kategorija;
    }

    public static Jelo getJeloById (int id) {

        switch (id) {
            case 0:
                return new Jelo(0, "ovsenakasa.jpg","Ovsena kaša", "Ovsena kaša sa bananama", "Doručak", "Ovas, banane", 120, 70.00, 3);
            case 1:
                return new Jelo(1, "pohovanapiletina.jpg","Pohovana piletina", "Piletina sa pomfritom", "Ručak", "Piletina, krompir", 470, 350.99, 4);
            case 2:
                return new Jelo(2, "ribljacorba.jpg","Riblja čorba", "Čorba sa ribom", "Večera", "Voda, riba, začini", 280, 180.90, 4);
            case 3:
                return new Jelo(3, "cokoladnikolac.jpg","Čokoladni kolač", "Kolač sa čokoladom", "Dezert", "Brašno, čokolada, šećer", 250, 220.00, 5);
            default:
                return null;
        }
    }
}
