package com.domaci.termin24domaci.activities;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.domaci.termin24domaci.R;
import com.domaci.termin24domaci.ReviewerTools;
import com.domaci.termin24domaci.servis23.KomentarService;
import com.domaci.termin24domaci.servis23.SimpleReceiver;

public class KomentarActivity extends AppCompatActivity {

    private TextView tvIme;
    private EditText etIme;
    private TextView tvKomentar;
    private EditText etKomentar;
    private Button bOK;
    private SimpleReceiver reciever;
    private SharedPreferences sharedPreferences;
    public static boolean dozvoliNotifikacije;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_komentar);
        setupReciever();
        tvIme = findViewById(R.id.tvIme);
        etIme = findViewById(R.id.etIme);
        tvKomentar = findViewById(R.id.tvKomenetar);
        etKomentar = findViewById(R.id.etKomentar);
        bOK = findViewById(R.id.bOK);
        bOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int comment = ReviewerTools.getConnectivityStatus(getApplicationContext());
                if (comment == ReviewerTools.TYPE_WIFI) {
                    String komentarPoruka = etKomentar.getText().toString();
                    Intent intent = new Intent(KomentarActivity.this, KomentarService.class);
                    intent.putExtra(KomentarService.KOMENTAR, komentarPoruka);
                    startService(intent);
                } else if (comment == ReviewerTools.TYPE_MOBILE) {
                    String komentarPoruka = etKomentar.getText().toString();
                    Intent intent = new Intent(KomentarActivity.this, KomentarService.class);
                    intent.putExtra(KomentarService.KOMENTAR, komentarPoruka);
                    startService(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Uredjaj nije povezan na internet", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onStop() {
        if (reciever != null){
            unregisterReceiver(reciever);
            reciever = null;
        }
        super.onStop();
    }

    private void setupReciever() {
        reciever = new SimpleReceiver();
        IntentFilter filter = new IntentFilter("KOMENTAR");
        registerReceiver(reciever, filter);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        consultPreferences();
    }

    private void consultPreferences() {
        dozvoliNotifikacije = sharedPreferences.getBoolean(getString(R.string.notif_settings), false);
    }
}
