package com.domaci.termin24domaci.fragmenti;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Spinner;

import com.domaci.termin24domaci.R;
import com.domaci.termin24domaci.model.JeloProvider;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;

public class DetailsFragment extends Fragment {

    private Spinner spinnerKategorija;
    private ListView lvSastojci;
    private ImageView imageView;
    private RatingBar ratingBar;
    private int jeloID;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.detail_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        spinnerKategorija = getView().findViewById(R.id.spinnerKategorija);
        lvSastojci = getView().findViewById(R.id.lvListaSastojaka);

        List<String> kategorija = Collections.singletonList(JeloProvider.getJeloById(jeloID).getKategorija());
        spinnerKategorija.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, kategorija));
        List<String> sastojci = Collections.singletonList(JeloProvider.getJeloById(jeloID).getSastojci());
        lvSastojci.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, sastojci));

        ImageView iv = getView().findViewById(R.id.imageView);
        InputStream is = null;
        try {
            is = getActivity().getAssets().open(JeloProvider.getJeloById(jeloID).getImeslike());
            Drawable drawable = Drawable.createFromStream(is, "");
            iv.setImageDrawable(drawable);
        } catch (Exception e) {
            Log.e("Detail Fragment", e.getMessage());
        }

        RatingBar rb = getView().findViewById(R.id.ratingBar);
        rb.setRating(JeloProvider.getJeloById(jeloID).getRating());
    }

    public void setJeloID(int id) {
        this.jeloID = id;
    }
}
