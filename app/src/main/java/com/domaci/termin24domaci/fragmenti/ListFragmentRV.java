package com.domaci.termin24domaci.fragmenti;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.domaci.termin24domaci.R;
import com.domaci.termin24domaci.RVAdapter;
import com.domaci.termin24domaci.model.JeloProvider;

import java.util.List;

public class ListFragmentRV extends Fragment {

    OnJeloClickedListener listener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.recycler_view, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnJeloClickedListener) context;
        } catch (ClassCastException e) {
            Toast.makeText(getContext(), "Implementiraj interfejs", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final List<String> imeJela = JeloProvider.getImeJela();

        RVAdapter adapter = new RVAdapter((OnJeloClickedListener) getActivity(), imeJela);
        RecyclerView rvLista = (RecyclerView) getActivity().findViewById(R.id.rvLista);
        rvLista.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvLista.setAdapter(adapter);
    }

    public interface OnJeloClickedListener {
        void onJeloClicked(int position);
    }
}
